import React from 'react';
import { Link } from 'react-router-dom';

export default function NoPage() {
  return (
    <>
      <div className="errorPage404">
        <h2 className="text-center color-green font-28">Oops 404</h2>
        <p className="font-22 text-center">Page not found! </p>
        <p className="text-center"><Link className="color-green" to="/">Click here & Go to Homepage</Link></p>
      </div>
    </>
  )
}