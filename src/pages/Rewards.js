import React from 'react';
import "./Rewards.css";
import VsqIcn from "../assets/vsqicm.svg";
import DaiIcon from "../assets/dai.svg";
import EthIcon from "../assets/eth.svg";
import PolygonIcon from "../assets/polygon.svg";
import DataCardComponent from '../components/DataCardComponent';
import { Link } from 'react-router-dom';

export default function Rewards() {
  const rewardsData = [
    {
      title: "VSQ",
      icon: VsqIcn,
      apy: "~12%",
      claimable: "120.00",
      action: "Re-lock",
      status: false
    },
    {
      title: "Dai",
      icon: DaiIcon,
      apy: "~8%",
      claimable: "19.00",
      action: "Claim",
      status: true
    },
    {
      title: "ETH",
      icon: EthIcon,
      apy: "~6%",
      claimable: "0.009",
      action: "Claim",
      status: true
    },
    {
      title: "Matic",
      icon: PolygonIcon,
      apy: "~38%",
      claimable: "29.00",
      action: "Claim",
      status: true
    }
  ]
  let rewardsStatus = true;
  if (Object.keys(rewardsData).length === 0) {
    rewardsStatus = false;
  }
  return (
    <>
      <section className="vsqRewardsPage page">
            <div className="vsqContainer">
                <h2 className="topHeading">Current Epoch <span className="color-green">1</span> ending in <span className="color-green">14 days, 11 hours and 59 minutes</span></h2>

                <div className="row ntvRow">
                    <div className="col-4">
                      <DataCardComponent
                        title= "Total Value Locked"
                        helpText= "Total Value Locked"
                        value= "$1,000.00"
                      />
                    </div>
                    <div className="col-4">
                      <DataCardComponent
                        title= "Total Claimable"
                        helpText= "Total Claimable"
                        value= "$168.00"
                      />
                    </div>
                    <div className="col-4">
                      <DataCardComponent
                        title= "Current Epoch"
                        helpText= "Current Epoch"
                        value= "~63%"
                      />
                    </div>
                </div>
                {rewardsStatus ?
                <div className="row ntvRow bigSmallGrid">
                  <div className="col-big">
                    <div className="vsqCard indexCard gradBox">
                      <p className="vsqCardTitle text-center font-22 color-green">Rewards</p>
                      {/* data table */}
                      <div className="tableResponsive">
                        <table className="w-100 rewardsTable">
                          <thead>
                            <tr>
                              <th className="font-18 color-grey font-400">&nbsp;</th>
                              <th className="font-18 color-grey font-400 text-left">APY</th>
                              <th className="text-right font-18 color-grey font-400">Claimable</th>
                              <th className="font-18 color-grey font-400">&nbsp;</th>
                            </tr>
                          </thead>
                          <tbody>
                            {rewardsData.map((item, index) =>
                            <tr key={index}>
                              <td>
                                <div className="flex align-center">
                                  <img src={item.icon} alt={item.title} />
                                  <span className="font-28">{item.title}</span>
                                </div>
                              </td>
                              <td className="font-18">{item.apy}</td>
                              <td className="text-right font-18">{item.claimable}</td>
                              <td className="text-right">
                                <div className="tableAction text-right">
                                  <button className={`cursor-pointer btn btn-sm font-18 ml-auto ${item.status === true ? "btn-green" : "btn-orange"}`}>{item.action}</button>
                                </div>
                              </td>
                            </tr>
                            )}
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                  <div className="col-small">
                    <div className="vsqCard indexCard gradBox">
                      <p className="vsqCardTitle text-center font-22 color-green">Info</p>
                      {/* infor lists */}
                      <div className="infoListOuter">
                        {/* sigle list */}
                        <div className="infoList">
                          <p className="font-18 color-grey">Total Revenue Earned</p>
                          <p className="font-28">~$0.00</p>
                        </div>
                        {/* sigle list */}
                        <div className="infoList">
                          <p className="font-18 color-grey">Current VSQ APY</p>
                          <p className="font-28">~12%</p>
                        </div>
                        {/* sigle list */}
                        <div className="infoList">
                          <p className="font-18 color-grey">Last Epoch APY</p>
                          <p className="font-28">~67%</p>
                        </div>
                        {/* sigle list */}
                        <div className="infoList">
                          <p className="font-18 color-grey">Pending Deposit</p>
                          <p className="font-28">1,000 VSQ</p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                :
                <div className="row ntvRow bigSmallGrid">
                  <div className="col-big w-100">
                    <div className="vsqCard indexCard gradBox">
                      <p className="vsqCardTitle text-center font-22 color-green">Rewards</p>
                      <div className="emptyRewards text-center">
                        <p className="font-22 text-center">You have no active locker to claim rewards</p>
                        <Link to="/" className="font-22 text-center color-green">Create Locker</Link>
                      </div>
                    </div>
                  </div>
                </div>
                }
            </div>
        </section>
    </>
  )
}
