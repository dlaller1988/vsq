import React, { useState } from 'react'
import "./Locker.css";
import BootsIcon from "../assets/metro-fire.svg";
import CloseIcon from "../assets/closeIcon.svg";
import BootsOrange from "../assets/metro-fire-orange.svg";
import Popup from 'reactjs-popup';
import 'reactjs-popup/dist/index.css';
import DataCardComponent from '../components/DataCardComponent';
import { Link } from 'react-router-dom';

export default function Locker() {
    const [open, setOpen] = useState(false);
    const closeModal = () => setOpen(false);

    const [showLocks, setShowLocks] = useState(false)
    const addLockFun = () => setShowLocks(true)

    if (open === true){
        document.body.classList.add('noScroll');
    } else {
        document.body.classList.remove('noScroll');
    }
    return (
    <>
        <section className="vsqLocker page">
            <div className="vsqContainer">
                <h2 className="topHeading">Current Epoch <span className="color-green">1</span> ending in <span className="color-green">14 days, 11 hours and 59 minutes</span></h2>

                <div className="row ntvRow">
                    <div className="col-4">
                        <DataCardComponent
                            title= "Locked Balance"
                            helpText= "Locked Balance"
                            value= "0.00 rsVSQ"
                        />
                    </div>
                    <div className="col-4">
                        <DataCardComponent
                            title= "Unlocked Balance"
                            helpText= "Unlocked Balance"
                            value= "0.00 rsVSQ"
                        />
                    </div>
                    <div className="col-4">
                        <DataCardComponent
                            title= "Pending Locks"
                            helpText= "Pending Locks"
                            value= "0.00 rsVSQ"
                        />
                    </div>
                </div>
                <div className="row ntvRow">
                    <div className="col-6">
                        <div className="vsqCard indexCard gradBox">
                            <p className="vsqCardTitle text-center font-22 color-green">VSQ Locker</p>
                            <div className="infoBox">
                                <h5 className="font-22">Lock VSQ to get rsVSQ</h5>
                                <p className="font-18 color-grey">Lock VSQ for a minimum of 30 days to receive rsVSQ, which earns platform fees generated from the vLabs ecosystem. <Link to="/#">Learn More</Link></p>
                            </div>
                            <div className="lockForm">
                                { showLocks ?
                                <p className="lockDepositDate font-18 color-green">Deposits Close: 15/11/22</p>
                                : 
                                <div className="lockButtons">
                                    <button className="btn font-18 active">30 days</button>
                                    <button className="btn font-18">90 days</button>
                                    <button className="btn font-18">180 days</button>
                                </div>
                                }
                                <div className="lockInput">
                                    <div className="inputBox">
                                        <input type="text" className="formControl lock" defaultValue="0.00"/>
                                        <button className="maxInput font-18">MAX</button>
                                    </div>
                                    <button onClick={addLockFun} id="addLock" className="btn btn-green w-100 font-28">Lock</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-6">
                        <div className="vsqCard indexCard gradBox">
                            <p className="vsqCardTitle text-center font-22 color-green">Current Locker</p>
                            { showLocks ?
                            <div className="lockDetails">
                                <p className="color-grey font-18 text-center">Balance</p>
                                <div className="gradBox lockCountBadge font-28">1,000 rsVSQ</div>
                                <ul>
                                    <li className="font-22 flex justify-between"><span>Duration</span><span>30 days</span></li>
                                    <li className="font-22 flex justify-between"><span>Expires</span><span>01/12/22 - 00:00</span></li>
                                    <li className="font-22 flex justify-between"><span>Pending Deposit</span><span>1,000 VSQ</span></li>
                                </ul>
                                <div className="lockInput">
                                    <button onClick={() => setOpen(o => !o)} id="boostLock" className="btn btn-orange w-100 font-28"><img src={BootsIcon} alt="VSQ" />Boost</button>
                                    <button id="unLock" className="btn w-100 font-28">Unlock</button>
                                </div>
                            </div>
                            :
                            <div className="emptyMsg">
                                <p className="font-22 text-center">You have no active locker</p>
                            </div>
                            }
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <Popup open={open} closeOnDocumentClick onClose={closeModal}>
            <div className="modal">
                <span className="close" onClick={closeModal}>
                    <img src={CloseIcon} alt="VSQ" />
                </span>
                <div className="modal-body">
                    <h5 className="m-0 font-28 text-center font-400 flex align-center justify-center"><img src={BootsOrange} alt="VSQ" /> Boost</h5>
                    {/* single boost */}
                    <div className="singleBoost activeBoost">
                        <p className="font-22">Active Boosts</p>
                        <div className="bootsBox">
                            <div className="row justify-between">
                                <p className="font-22 m-0">VSQ Diamond Hands</p>
                                <p className="font-22 m-0">Expires in 179 days</p>
                            </div>
                            <div className="boosts">
                                <div className="boost">
                                    <span className="font-12">VSQ Boost</span>
                                    <span className="big font-18">1.2x</span>
                                </div>
                                <div className="boost emptyBoost"></div>
                            </div>
                        </div>
                    </div>
                    {/* single boost */}
                    <div className="singleBoost availableBoost">
                        <p className="font-22">Available Boosts</p>
                        <div className="bootsBox">
                            <div className="row justify-between">
                                <p className="font-22 m-0">VSQ Early Adopter</p>
                                <p className="font-22 m-0">30 days</p>
                            </div>
                            <div className="boosts">
                                <div className="boost emptyBoost"></div>
                                <div className="boost emptyBoost"></div>
                            </div>
                            <div className="boostAction text-right">
                                <button className="btn btn-green btn-sm font-18 w-auto">Apply</button>
                            </div>
                        </div>
                    </div>
                    <div className="lmLink">
                        <p className="font-22 color-grey text-center text-underline m-0">Learn More about Boosts</p>
                    </div>
                </div>
            </div>
        </Popup>
    </>
  )
}
