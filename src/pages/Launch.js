import React from 'react'
import "./Bonds.css";
import Proxy from "../assets/proxy.svg"
import L30x from "../assets/30x.svg"

export default function Launch() {
  return (
    <>
      <section className="vsqLaunch page">
        <div className="vsqContainer">
          {/* liveLaunches box */}
          <div className="liveLaunches">
            <p className="font-28">Live Launches</p>
            <div className="launchBox">
              <div className="emptyLounch gradBox">
                <p className="m-0 color-grey font-28 text-center">No launches available</p>
              </div>
            </div>
          </div>
          {/* upcomming launches boxes */}
          <div className="upcomingLaunches">
            <p className="font-28">Upcoming Launches</p>
            <div className="row justify-between">
              <div className="upLaunchBox">
                <img src={L30x} alt="VSQ" />
              </div>
              <div className="upLaunchBox">
                <img src={Proxy} alt="VSQ" />
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  )
}
