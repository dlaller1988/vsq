import React, { useState } from 'react'
import "./Launch.css";
import DataCardComponent from '../components/DataCardComponent';
import { Link } from 'react-router-dom';
import SwapIcon from '../assets/swapIcon.svg'

export default function Bonds() {
  const [showLocks, setShowLocks] = useState(false)
  const addLockFun = () => setShowLocks(true)
  return (
    <>
      <section className="vsqBondPage page">
        <div className="vsqContainer">
            <h2 className="topHeading">Current Epoch <span className="color-green">1</span> ending in <span className="color-green">14 days, 11 hours and 59 minutes</span></h2>
            <div className="row ntvRow">
                <div className="col-4">
                    <DataCardComponent
                        title= "Bond Price"
                        helpText= "Bond Price"
                        value= "$0.10"
                    />
                </div>
                <div className="col-4">
                    <DataCardComponent
                        title= "Total Available"
                        helpText= "Total Available"
                        value= "100,000 VSQ"
                    />
                </div>
                <div className="col-4">
                    <DataCardComponent
                        title= "Total Sold"
                        helpText= "Total Sold"
                        value= "0 VSQ"
                    />
                </div>
              </div>
              <div className="row ntvRow bigSmallGrid">
                <div className="col-big">
                  <div className="vsqCard indexCard gradBox">
                    <p className="vsqCardTitle text-center font-22 color-green">Buy Bond</p>
                    <div className="infoBox">
                      <h5 className="font-22">Buy VSQ for Matic</h5>
                      <p className="font-18 color-grey">Buy and lock VSQ at a discount, you must have an active locker to take advantage of bonds.<Link to="/#">Learn More</Link></p>
                    </div>
                    <div className="lockForm bondForm">
                      <div className="lockInput">
                        <button className="swapBtn">
                          <img src={SwapIcon} alt="VSQ" />
                        </button>
                        <div className="inputBox">
                          <input type="text" className="formControl lock polyIcon" defaultValue="0.00"/>
                        </div>
                        <div className="inputBox">
                          <input type="text" className="formControl lock vsqIcon" defaultValue="0.00"/>
                        </div>
                        <button onClick={addLockFun} id="addLock" className="btn btn-default w-100 font-28">Buy</button>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-small">
                  <div className="vsqCard indexCard gradBox">
                    <p className="vsqCardTitle text-center font-22 color-green">Active Locker</p>
                    { showLocks ?
                    <div className="lockDetails">
                        <p className="color-grey font-18 text-center">Balance</p>
                        <div className="gradBox lockCountBadge font-28">1,000 rsVSQ</div>
                        <ul>
                            <li className="font-22 flex justify-between"><span>Duration</span><span>30 days</span></li>
                            <li className="font-22 flex justify-between"><span>Expires</span><span>01/12/22 - 00:00</span></li>
                            <li className="font-22 flex justify-between"><span>Pending Deposit</span><span>1,000 VSQ</span></li>
                            <li className="font-22 flex justify-between"><span>Deposits Close</span><span>15/11/22</span></li>
                        </ul>
                    </div>
                    :
                    <div className="emptyRewards text-center">
                      <p className="font-22 text-center">You have no active locker</p>
                      <Link to="/" className="font-22 text-center color-green">Create Locker</Link>
                    </div>
                    }
                  </div>
                </div>
            </div>
          </div>
        </section>
    </>
  )
}
