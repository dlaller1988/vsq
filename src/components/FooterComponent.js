import React from 'react'
import "./FooterComponent.css";
import { Link } from 'react-router-dom';

export default function FooterComponent() {
  return (
    <>
      <section className="vsqFooter">
        <div className="vsqContainerFull">
          <div className="row justify-between">
            <ul>
              <li><span className="font-18">&copy; 2022 Vesq</span></li>
              <li><Link className="font-18" to="/">· v2.0.0  </Link></li>
              <li><Link className="font-18" to="/">· Terms</Link></li>
              <li><Link className="font-18" to="/">· Privacy</Link></li>
            </ul>
            <ul>
              <li><Link className="font-18" to="/">Discord</Link></li>
              <li><Link className="font-18" to="/">· Twitter</Link></li>
              <li><Link className="font-18" to="/">· Mirror</Link></li>
              <li><Link className="font-18" to="/">· Gitbook</Link></li>
            </ul>
          </div>
        </div>
      </section>
    </>
  )
}
