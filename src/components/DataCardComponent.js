import React from 'react';
import ReactTooltip from 'react-tooltip';
import TooltipIcon from "../assets/tooltipIcon.svg";

export default function DataCardComponent(props) {
  return (
    <>
        <div className="vsqCard indexCard gradBox comCard">
            <p className="color-grey font-18">{props.title}<span className="tooltipIcon" data-tip={props.helpText} data-for="unLockedBalance"><img src={TooltipIcon} alt="VSQ" /></span></p>
            <ReactTooltip id="unLockedBalance" type="light" alt="VSQ" />
            <h4>{props.value}</h4>
        </div>
    </>
  )
}