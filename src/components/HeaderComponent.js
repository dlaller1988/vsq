import React, {useState, useEffect} from "react";
import Logo from "../assets/logo.svg";
import { Link } from "react-router-dom";
import "./HeaderComponent.css";
import VsqToken from "../assets/VSQToken.svg";
import CloseIcon from "../assets/closeIcon.svg";
import CircleLogo from "../assets/ac-vsq-logo.svg";
import CopyIcon from "../assets/copyIcon.svg";
import DisconnectIcon from "../assets/closedIcon.svg";
import PolygonMaticLogo from "../assets/polygon-matic-logo.svg";
import VsqGreen from "../assets/vsqSmallGren.svg";
import PolyLogo from "../assets/polygon-logo.svg";
import MaticLogo from "../assets/matic-logo.svg";
import VsqOrange from "../assets/vsqSmallOrange.svg";
import FoxIcon from "../assets/fox.svg";
import Popup from 'reactjs-popup';
import 'reactjs-popup/dist/index.css';

export default function HeaderComponent() {
  
  const [openMigrate, setOpenMigrate] = useState(false);
  const closeMigrate = () => setOpenMigrate(false);

  const [openAccount, setOpenAccount] = useState(false);
  const closeAccount = () => setOpenAccount(false);
  
  const [openVsq, setOpenVsq] = useState(false);
  const closeVsq = () => setOpenVsq(false);
  
  const [openNetwork, setOpenNetwork] = useState(false);
  const closeNetwork = () => setOpenNetwork(false);

  const [step2, setStep2] = useState(false);
  const [step3, setStep3] = useState(false);

  const [hamMenu, setHamMenu] = useState(false);
  const toggleHamMenu = () => {
    setHamMenu(current => !current);
  };
  const checkStatus = hamMenu === true ? "checked" : "";

  // get window size
  const [windowSize, setWindowSize] = useState(getWindowSize());
  useEffect(() => {
    function handleWindowResize() {
      setWindowSize(getWindowSize());
    }
    window.addEventListener('resize', handleWindowResize);
    return () => {
      window.removeEventListener('resize', handleWindowResize);
    };
  }, []);
  function getWindowSize() {
    const {innerWidth, innerHeight} = window;
    return {innerWidth, innerHeight};
  }
  // get window size end

  // remove scroll from body
  if (openMigrate === true || openAccount === true || openVsq === true || openNetwork === true){
    document.body.classList.add('noScroll');
  } else if ( windowSize.innerWidth <=991 && hamMenu === true){
    document.body.classList.add('noScroll');
  }else{
    document.body.classList.remove('noScroll');
  }
  // remove scroll from body end

  return (
    <>
      <div onClick={toggleHamMenu} className={`m-show backDrop ${hamMenu===true ? "open" : "close"}`}></div>
      <header className="vsqHeader">
        <div className="vsqContainerFull">
          {/* navbar */}
          <nav className="row justify-between align-center">
            <div className="leftMenu">
              <div className="row align-center">
                {/* brand Logo */}
                <div className="vsqLogo">
                  <Link to="/">
                    <img src={Logo} alt="VSQ" />
                  </Link>
                </div>
                <div className={`leftNavBar ${hamMenu===true ? "open" : "close"}`}>
                    <ul>
                      <li><Link onClick={toggleHamMenu} className="font-18 color-grey" to="/">Locker</Link></li>
                      <li><Link onClick={toggleHamMenu} className="font-18 color-grey" to="rewards">Rewards</Link></li>
                      <li><Link onClick={toggleHamMenu} className="font-18 color-grey" to="bonds">Bonds</Link></li>
                      <li><Link onClick={toggleHamMenu} className="font-18 color-grey" to="launch">Launch</Link></li>
                    </ul>
                </div>
              </div>
            </div>
            <div className="rightMenu">
              <ul>
                <li><span className="gradBox greenHover cursor-pointer font-0 font-18" onClick={() => setOpenVsq(o => !o)} ><img src={VsqToken} alt="VSQ" /> VSQ</span></li>
                <li><span className="gradBox greenHover cursor-pointer font-0 font-18" onClick={() => setOpenNetwork(o => !o)} ><img src={PolygonMaticLogo} alt="VSQ" /> Polygon</span></li>
                <li><span className="gradBox greenHover cursor-pointer font-18" onClick={() => setOpenAccount(o => !o)} >0x22…74A1</span></li>
                <li className="m-show">
                  <div className="hamBurger">
                    <span id="menu__toggle" type="checkbox" className={checkStatus}></span>
                    <label onClick={toggleHamMenu} className="menu__btn" htmlFor="menu__toggle">
                      <span></span>
                    </label>
                  </div>
                </li>
              </ul>
            </div>
          </nav>
        </div>
      </header>

      <Popup className="migratePopup" open={openMigrate} closeOnDocumentClick onClose={closeMigrate}>
        <div className="modal">
          <span className="close" onClick={closeMigrate}>
              <img src={CloseIcon} alt="VSQ" />
          </span>
          <div className="modal-body migrateBody">
            <h5 className="m-0 font-28 text-center font-400 flex align-center justify-center">Migrate to v2</h5>
            <div className="stepBox row ntvRow">
              {/* single step */}
              <div className={`singleStep col-4 row align-center ${step2 === false && step3 === false ? "active" : ""}`}>
                <div className="count"><span className="grd font-28">1</span></div>
                <div className="stepTitle">
                  <p className="font-28 m-0">Overview</p>
                  <p className="font-18 color-grey">Migration Info</p>
                </div>
              </div>
              {/* single step */}
              <div className={`singleStep col-4 row align-center ${step2 === true && step3 === false ? "active" : ""}`}>
                <div className="count"><span className="grd font-28">2</span></div>
                <div className="stepTitle">
                  <p className="font-28 m-0">Approve</p>
                  <p className="font-18 color-grey">Token Approvals</p>
                </div>
              </div>
              {/* single step */}
              <div className={`singleStep col-4 row align-center ${step2 === false && step3 === true ? "active" : ""}`}>
                <div className="count"><span className="grd font-28">3</span></div>
                <div className="stepTitle">
                  <p className="font-28 m-0">Migrate</p>
                  <p className="font-18 color-grey">Migrate to v2</p>
                </div>
              </div>
            </div>
            {/* for step 1 */}
            {!step2 && !step3 &&
            <div className="stepContent">
              <p className="color-grey font-22">Migrating to V2 gives you a bunch of cool benefits like revenue sharing with rsVSQ. Note that all tokens must be migrated and locked in order to receive VSQ emissions and rewards from revenue sharing. <Link className="text-underline" to={"/"}>Learn More</Link></p>
              <div className="stepAction">
                <button onClick={()=> setStep2(true)} className="btn btn-green font-28 mx-auto">Approve</button>
              </div>
            </div>
            }
            {/* for step 2 */}
            {step2 && !step3 &&
            <div className="stepContent">
              <div className="vsqTokenList">
                {/* single token */}
                <div className="singleToken row align-center justify-between gradBox greenHover cursor-pointer">
                  <div className="leftToken row align-center">
                    <div className="tokenIcon">
                      <img src={VsqGreen} alt="VSQ" />
                    </div>
                    <div className="tokenTitle"><span className="font-28">VSQ v1</span></div>
                  </div>
                  <div className="tokenRight row align-center">
                    <button className="font-18 btn btn-green btn-sm m-mx-auto">Approve</button>
                  </div>
                </div>
                {/* single token */}
                <div className="singleToken row align-center justify-between gradBox greenHover cursor-pointer">
                  <div className="leftToken row align-center">
                    <div className="tokenIcon">
                      <img src={VsqGreen} alt="VSQ" />
                    </div>
                    <div className="tokenTitle"><span className="font-28">VSQ v2</span></div>
                  </div>
                  <div className="tokenRight row align-center">
                    <button className="font-18 btn btn-green btn-sm m-mx-auto">Approve</button>
                  </div>
                </div>
              </div>
              <div className="stepAction">
                <button onClick={()=> {setStep3(true); setStep2(false);}} className="btn btn-green font-28 mx-auto">Migrate</button>
              </div>
            </div>
            }
            {/* for step 3 */}
            {!step2 && step3 &&
            <div className="stepContent">
              <div className="vsqTokenList">
                {/* single token */}
                <div className="singleToken row align-center justify-between gradBox greenHover cursor-pointer">
                  <div className="leftToken row align-center">
                    <div className="tokenIcon">
                      <img src={VsqGreen} alt="VSQ" />
                    </div>
                    <div className="tokenTitle"><span className="font-28">VSQ v1</span></div>
                  </div>
                  <div className="tokenRight row align-center">
                    <span className="tokenPrice color-green font-28">MAX</span>
                    <span className="tokenPrice font-28">0.00</span>
                  </div>
                </div>
                {/* single token */}
                <div className="singleToken row align-center justify-between gradBox greenHover cursor-pointer">
                  <div className="leftToken row align-center">
                    <div className="tokenIcon">
                      <img src={VsqGreen} alt="VSQ" />
                    </div>
                    <div className="tokenTitle"><span className="font-28">VSQ v2</span></div>
                  </div>
                  <div className="tokenRight row align-center">
                    <span className="tokenPrice color-green font-28"></span>
                    <span className="tokenPrice font-28">0.00</span>
                  </div>
                </div>
              </div>
              <div className="stepAction">
                <button onClick={()=> {setStep3(false); setStep2(false); closeMigrate()}} className="btn btn-green font-28 mx-auto">Complete Migration</button>
              </div>
            </div>
            }
          </div>
        </div>
      </Popup>
      <Popup open={openNetwork} closeOnDocumentClick onClose={closeNetwork}>
        <div className="modal">
          <span className="close" onClick={closeNetwork}>
              <img src={CloseIcon} alt="VSQ" />
          </span>
          <div className="modal-body">
          <h5 className="m-0 font-28 text-center font-400 flex align-center justify-center">Network</h5>
            <div className="vsqTokenList">
              {/* single token */}
              <div className="singleToken row align-center justify-between gradBox greenHover cursor-pointer">
                <div className="leftToken row align-center">
                  <div className="tokenIcon">
                    <img src={PolyLogo} alt="VSQ" />
                  </div>
                  <div className="tokenTitle"><span className="font-28">Polygon</span></div>
                </div>
              </div>
              {/* single token */}
              <div className="singleToken row align-center justify-between gradBox greenHover cursor-pointer">
                <div className="leftToken row align-center">
                  <div className="tokenIcon">
                    <img src={MaticLogo} alt="VSQ" />
                  </div>
                  <div className="tokenTitle"><span className="font-28">Matic</span></div>
                </div>
                <div className="tokenRight row align-center">
                  <span className="tokenPrice font-28">$0.15</span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Popup>
      <Popup  open={openAccount} closeOnDocumentClick onClose={closeAccount} className="accountModal">
        <div className="modal">
          <span className="close" onClick={closeAccount}>
              <img src={CloseIcon} alt="VSQ" />
          </span>
          <div className="modal-body">
            <div className="text-center">
              <img className="width-80" src={CircleLogo} alt="VSQ" />
              <div className="accountData">
                <p className="font-28 text-center m-0">0x22…74A1</p>
                <p className="font-28 text-center m-0 color-green">0 VSQ · 0 rsVSQ</p>
                <div className="row justify-center accountAction">
                  <span className="vsqAction text-center font-22 cursor-pointer greenHover">
                    <img src={CopyIcon} alt="VSQ" />
                    Copy
                  </span>
                  <span className="vsqAction text-center font-22 cursor-pointer greenHover">
                    <img src={DisconnectIcon} alt="VSQ" />
                    Disconnect
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Popup>
      <Popup open={openVsq} closeOnDocumentClick onClose={closeVsq}>
        <div className="modal">
          <span className="close" onClick={closeVsq}>
              <img src={CloseIcon} alt="VSQ" />
          </span>
          <div className="modal-body">
            <h5 className="m-0 font-28 text-center font-400 flex align-center justify-center">Vesq Protocol Tokens</h5>
            {/* token list */}
            <div className="vsqTokenList">
              {/* single token */}
              <div className="singleToken row align-center justify-between gradBox greenHover cursor-pointer">
                <div className="leftToken row align-center">
                  <div className="tokenIcon">
                    <img src={VsqGreen} alt="VSQ" />
                  </div>
                  <div className="tokenTitle"><span className="font-28">VSQ</span></div>
                  <div className="tokenSub"><span className="font-18 color-grey">v2</span></div>
                </div>
                <div className="tokenRight row align-center">
                  <span className="tokenPrice font-28">0.00</span>
                  <div className="foxIcon">
                    <img src={FoxIcon} alt="VSQ" />
                  </div>
                </div>
              </div>
              {/* single token */}
              <div className="singleToken row align-center justify-between gradBox greenHover cursor-pointer">
                <div className="leftToken row align-center">
                  <div className="tokenIcon">
                    <img src={VsqOrange} alt="VSQ" />
                  </div>
                  <div className="tokenTitle"><span className="font-28">rsVSQ</span></div>
                  <div className="tokenSub"><span className="font-18 color-grey">Revenue Sharing VSQ</span></div>
                </div>
                <div className="tokenRight row align-center">
                  <span className="tokenPrice font-28">0.00</span>
                  <div className="foxIcon">
                    <img src={FoxIcon} alt="VSQ" />
                  </div>
                </div>
              </div>
              {/* token end */}
            </div>
            {/* token action */}
            <div className="tokenAction">
              <button className="btn btn-green font-28">Buy VSQ</button>
              <button className="btn btn-orange font-28" onClick={()=>{setOpenMigrate(o => !o); closeVsq();}}>Migrate v1 - v2</button>
            </div>
            {/* token action end */}
          </div>
        </div>
      </Popup>
    </>
  );
}
