import './App.css';
import { Routes, Route } from "react-router-dom";
import Locker from './pages/Locker';
import Bonds from './pages/Bonds';
import Launch from './pages/Launch';
import Rewards from './pages/Rewards';
import NoPage from './pages/404';

function App() {
  return (
    <>
      <Routes>
        <Route path="/" element={<Locker />} />
          <Route path="rewards" element={<Rewards />} />
          <Route path="bonds" element={<Bonds />} />
          <Route path="launch" element={<Launch />} />
          <Route path="*" element={<NoPage />} />
      </Routes>
    </>
  );
}

export default App;
